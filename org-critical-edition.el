;;; org-critical-edition.el --- A high level interface in Org Mode for the LaTeX package reledmac    -*- lexical-binding: t; -*-

;; Copyright (C) 2021--2024  Juan Manuel Macías

;; Author: Juan Manuel Macías <maciaschain@posteo.net>
;; URL: https://gitlab.com/maciaschain/org-critical-edition
;; Version: 0.9
;; Package-Requires: ((emacs "27.2") (org "9.4") (posframe "1.0.2"))
;; Keywords: org-mode, reledmac, critical edition, LaTeX, typography

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This package lets you prepare a philological critical edition in
;; Org Mode. The natural output is LaTeX with the reledmac package
;; (see https://github.com/maieul/ledmac and
;; https://www.ctan.org/pkg/reledmac)

;;; Code:

(provide 'org-critical-edition)

;; packages required
  (require 'org)
  (require 'posframe)

;; face and variables

(defface org-critical-edition-edtext-face
  '((t :foreground "chocolate" :bold t))
  "")

(defface org-critical-edition-nested-face
  '((t :foreground "chocolate" :bold t :underline t))
  "")

(defvar org-critical-edition-alias-xfootnote '(("A" "Afootnote")
					       ("B" "Bfootnote")
					       ("C" "Cfootnote"))
  "Default value for critical footnotes. It consists of a
  list of lists where each element is formed by two strings: the
  keyword for the notes series as is used in
  `org-critical-edition' and the name of the series as is
  understood by `reledmac'")

(defvar org-critical-edition-alias-xendnote '(("EA" "Aendnote")
					      ("EB" "Bendnote")
					      ("EC" "Cendnote"))
  "Default value for critical end footnotes. It consists of a
  list of lists where each element is formed by two strings: the
  keyword for the end notes series as is used in
  `org-critical-edition' and the name of the series as is
  understood by `reledmac'")

;;;###autoload
(define-minor-mode org-critical-edition-mode
  "An org minor mode for critical editions"
  :lighter "oce"
  :global nil

  (if org-critical-edition-mode
      (progn
	(advice-add 'org-activate-links
		    :before
		    #'org-critical-edition-hide-critical-notes)
	(advice-add 'org-activate-links
		    :before
		    #'org-critical-edition-hide-nested-notes)
	(highlight-regexp
	 "<!<!\\(\\(?:[^!><!\\]\\|\\\\\\(?:\\\\\\\\\\)*[!><!]\\|\\\\+[^!><!]\\)+\\)!>\\(?:<!\\([^z-a]+?\\)!>\\)?!>"
	 'org-critical-edition-nested-face)
	(org-restart-font-lock))
    (advice-remove 'org-activate-links
		   #'org-critical-edition-hide-critical-notes)
    (advice-remove 'org-activate-links
		   #'org-critical-edition-hide-nested-notes)
    (org-restart-font-lock)))

;;;###autoload
(defun org-critical-edition-xfootnote-replace ()
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward (concat
			       "\s*:"
			       "\\("
			       (regexp-opt (mapcar
					    (lambda (e)
					      (car e))
					    org-critical-edition-alias-xfootnote))
			       "\\)"
			       "\\(([^()]+)\\)?"
			       "\s*")
			      nil t)
      (replace-match
       (concat
	"\\\\"
	(cadr
	 (assoc (match-string 1)
		org-critical-edition-alias-xfootnote))
	(pcase (match-string-no-properties 2)
	  (`nil nil)
	  ((pred stringp) (replace-regexp-in-string
			   "(\\|)"
			   ""
			   (format "[%s]" (match-string 2)))))
	"{")
       t nil))))

;;;###autoload
(defun org-critical-edition-xendnote-replace ()
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward (concat
			       "\s*:"
			       "\\("
			       (regexp-opt (mapcar
					    (lambda (e)
					      (car e))
					    org-critical-edition-alias-xendnote))
			       "\\)"
			       "\s*")
			      nil t)
      (replace-match
       (concat
	"\\\\"
	(cadr
	 (assoc (match-string 1)
		org-critical-edition-alias-xendnote))
	"{")
       t nil))))

;;;###autoload
(defun org-critical-edition-replace (before after)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward before nil t)
      (replace-match after t nil))))

;;;###autoload
(defun org-critical-edition-resolve-nested-notes-in-note (desc target)
  (if (string-match ":nested" desc)
      (with-temp-buffer
	(insert desc)
	(save-excursion
	  (goto-char (point-min))
	  (while (re-search-forward ":nested((" nil t)
	    (let* ((case-fold-search nil)
		   (nested-lem-from (point))
		   (nested-lem-to (save-excursion
				    (re-search-forward "))" nil t)
				    (- (point) 2)))
		   (nested-lemma-string (buffer-substring-no-properties nested-lem-from nested-lem-to))
		   (nested-not-from (save-excursion
				      (re-search-forward "!(" nil t)
				      (point)))
		   (nested-not-to (save-excursion
				    (goto-char (- nested-not-from 1))
				    (org-element--parse-paired-brackets ?\()
				    (- (point) 1)))
		   (nested-not-string (buffer-substring-no-properties nested-not-from nested-not-to))
		   (nested-not-string-conv (with-temp-buffer
					     (insert nested-not-string)
					     (org-critical-edition-replace "\s*:lemma\s*" "\\\\lemma{")
					     (org-critical-edition-xfootnote-replace)
					     (org-critical-edition-xendnote-replace)
					     (org-critical-edition-replace "\s*;;\s*" "}")
					     ;; eliminar marca de fin de versos 
					     (org-critical-edition-replace "\n" "%\n")
					     (buffer-string))))
	      (with-temp-buffer
		(insert target)
		(if (string-match-p "\\(.+\\)/\\([[:digit:]]+\\)" nested-lemma-string)
		    (let ((str (progn (string-match "\\(.+\\)/\\([[:digit:]]+\\)" nested-lemma-string)
				      (match-string 1 nested-lemma-string)))
			  (num (progn (string-match "\\(.+\\)/\\([[:digit:]]+\\)" nested-lemma-string)
				      (string-to-number (match-string 2 nested-lemma-string)))))
		      (save-excursion
			(goto-char (point-min))
			(while (re-search-forward (concat "\\(" str "\\)") nil t num)
			  (replace-match (concat "\\\\edtext{"
						 "\\1"
						 "}{"
						 (replace-regexp-in-string
						  "\\\\" "\\\\\\\\" nested-not-string-conv)
						 "}") 
                                                 t nil))))
		  (save-excursion
		    (goto-char (point-min))
		    (while (re-search-forward (concat "\\(" nested-lemma-string "\\)") nil t)
		      (replace-match (concat "\\\\edtext{"
					     "\\1"
					     "}{"
					     (replace-regexp-in-string
					      "\\\\" "\\\\\\\\" nested-not-string-conv)
					     "}") 
                                              t nil))))
		(setq target (buffer-string))))))
	(let ((reg-from (save-excursion
			  (goto-char (point-min))
			  (re-search-forward ":nested" nil t)
			  ;; (beginning-of-line)
			  (- (point) 7)))
	      (reg-to (save-excursion
			(goto-char (point-max))
			(point))))
	  (delete-region reg-from reg-to))
	(setq targ target)
	(buffer-string))
    (setq targ target)
    desc))

(defun org-critical-edition--get-visible ()
  (when (region-active-p)
    (let ((region (buffer-substring-no-properties (region-beginning) (region-end))))
      (with-temp-buffer
	(insert region)
	(save-excursion
	  (goto-char (point-min))
	  (while (re-search-forward org-link-bracket-re nil t)
	    (let ((w (buffer-substring-no-properties (match-beginning 1) (match-end 1))))
	      (replace-match
	       (replace-regexp-in-string
		"edtext:" ""
		w)
	       t nil))))
	(buffer-string)))))

(defun org-critical-edition-notes-to-nested-in-region ()
  "TODO"
  (interactive)
  (when (region-active-p)
    (setq org-critical-edition-notes-in-region nil)
    (setq reg-text (org-critical-edition--get-visible))
    (setq org-critical-edition-origin-buffer (current-buffer))
    (setq org-critical-edition-current-w-conf (random 1000))
    (window-configuration-to-register org-critical-edition-current-w-conf)
    (save-restriction
      (narrow-to-region (region-beginning) (region-end))
      (save-excursion
	(goto-char (point-min))
	(while (re-search-forward org-link-bracket-re nil t)
	  (let ((w (buffer-substring-no-properties (match-beginning 1) (match-end 1)))
		(note (buffer-substring-no-properties (match-beginning 2) (match-end 2))))
	    (replace-match
	     (replace-regexp-in-string
	      "edtext:" ""
	      w)
	     t nil)
	    (push (cons (replace-regexp-in-string "edtext:" "" w)
			note)
		  org-critical-edition-notes-in-region)))))
    (setq edtext (concat "edtext:" reg-text))
    (when (get-buffer "edit-critical-note")
      (kill-buffer "edit-critical-note"))
    (get-buffer-create "edit-critical-note")
    (set-buffer "edit-critical-note")
    (org-mode)
    (setq header-line-format (format "Edit: %s" reg-text))
    (unless (bound-and-true-p org-critical-edition-mode)
      (org-critical-edition-mode))
    (pop-to-buffer "edit-critical-note")
    (mapc (lambda (elt)
	    (let ((inner-nested-list))
	      (with-temp-buffer
		(insert (cdr elt))
		(save-excursion
		  (goto-char (point-min))
		  (while (re-search-forward "\\(:nested(\\)" nil t)
		    (let* ((nested-beg (match-beginning 1))
			   (str (save-excursion
				  (goto-char (match-end 1))
				  (org-element--parse-paired-brackets ?\()))
			   (nested-end (save-excursion
					 (re-search-forward "!(" nil t)
					 (forward-char -1)
					 (org-element--parse-paired-brackets ?\()
					 (point)))
			   (n-str (buffer-substring-no-properties nested-beg nested-end)))
		      (push (cons str n-str) inner-nested-list)
		      (setq inner-nested-list (sort
					       inner-nested-list
					       (lambda (a b) (string> (car a) (car b)))))
		      (delete-region nested-beg nested-end)
		      (setcdr elt (buffer-string))))))
	      (let* ((nested (format ":nested((%s))" (car elt)))
		     (notes (format "!(\n%s\n)" (cdr elt)))
		     (format (concat nested notes)))
		(insert format)
		(setq inner-nested-list (mapcar (lambda (el)
						  (cdr el))
						inner-nested-list))
		(insert (mapconcat #'identity inner-nested-list "\n")))))
	  org-critical-edition-notes-in-region)
    ;; arrange notes on several lines for easier reading.
    (org-critical-edition-replace "\\(;;\s*\\)" "\\1\n")	   
    (org-critical-edition-replace "\\(:nested\\)" "\n\\1")
    (org-critical-edition-replace "\\(!(\\)" "\n\\1")
    (highlight-regexp "\\(:latex\\|:lemma\\|;;\\|:[A-Z]+\\)" 'hi-red-b)))

;;;###autoload
(org-link-set-parameters "edtext"
			 :display 'full
			 :follow (lambda (target)(occur (format "edtext:%s" target)))
			 :face 'org-critical-edition-edtext-face
			 :export (lambda (target desc format)
				   (cond
				    ((eq format 'latex)
				     (setq target
					   (with-temp-buffer
					     (insert target)
					     (org-critical-edition-replace "<!<!@nested:" "\\\\edtext{")
					     (org-critical-edition-replace "!><!" "}{")
					     (org-critical-edition-replace "!>!>" "}")
					     (org-critical-edition-replace "\s*:lemma\s*" "\\\\lemma{")
					     (org-critical-edition-xfootnote-replace)
					     (org-critical-edition-xendnote-replace)
					     (org-critical-edition-replace "\s*;;\s*" "}")
					     (org-critical-edition-replace "}\n" "} %\n")
					     (org-critical-edition-replace "\\(xxref{.+}{.+}\\)" "\\1\\\\ignorespaces")
					     (buffer-string)))
				     (let* ((case-fold-search nil)
					    (arg-edtext-pre (org-critical-edition-resolve-nested-notes-in-note desc target))
					    (arg-edtext
					     (if (string-match "\\(:latex\\)\s*\\(.+\\)"  desc)
						 (format "%s" (match-string 2 desc))
					       (with-temp-buffer
						 (insert arg-edtext-pre)
						 (org-critical-edition-replace "\s*:lemma\s*" "\\\\lemma{")
						 (org-critical-edition-xfootnote-replace)
						 (org-critical-edition-xendnote-replace)
						 (org-critical-edition-replace "\s*;;\s*" "}")
						 (org-critical-edition-replace "}\n" "} %\n")
						 (org-critical-edition-replace "\n" " %\n")
						 (org-critical-edition-replace "\\(xxref{.+}{.+}\\)" "\\1\\\\ignorespaces")
						 (buffer-string)))))
				       (concat
					"\\edtext{"
					(replace-regexp-in-string "edtext:" "" targ)
					"}{" arg-edtext "}"))))))

;;;###autoload
(defun org-critical-edition-hide-critical-notes (&rest _ignore)
  (save-excursion
    (goto-char (point-min))
    (while  (re-search-forward org-bracket-link-regexp nil t)
      (when (string-match-p "edtext:\\(.+\\)" (match-string 1))
	(let* ((label (match-string 1))
	       (edtext (save-match-data
			 (when (string-match "edtext:\\(.+\\)" label)
			   (match-string 1 label))))
	       (start1 (- (match-beginning 1) 2))
	       (end1 (+ (length "edtext:") (match-beginning 1)))
	       (start2 (- (match-beginning 2) 2))
	       (end2 (+ (match-end 2) 2)))
	  (add-text-properties start1 end1 '(invisible t))
	  (add-text-properties start2 end2 '(invisible t)))))))

;; ;;;###autoload
;; (defun org-critical-edition-show-notes ()
;;   (interactive)
;;   (advice-remove 'org-activate-links #'org-critical-edition-hide-critical-notes)
;;   (org-restart-font-lock))

;; ;;;###autoload
;; (defun org-critical-edition-hide-notes ()
;;   (interactive)
;;   (advice-add 'org-activate-links :before #'org-critical-edition-hide-critical-notes)
;;   (org-restart-font-lock))

;;;###autoload
(defun org-critical-edition-show-notes ()
  (interactive)
  (advice-remove 'org-activate-links #'org-critical-edition-hide-critical-notes)
  (advice-remove 'org-activate-links #'org-critical-edition-hide-nested-notes)
  (org-restart-font-lock))

;;;###autoload
(defun org-critical-edition-hide-notes ()
  (interactive)
  (advice-add 'org-activate-links :before #'org-critical-edition-hide-critical-notes)
  (advice-add 'org-activate-links :before #'org-critical-edition-hide-nested-notes)
  (org-restart-font-lock))

;;;###autoload
(defun org-critical-edition-hide-nested-notes (&rest _ignore)
  (save-excursion
    (goto-char (point-min))
    (while  (re-search-forward "<!<!\\(\\(?:[^!><!\\]\\|\\\\\\(?:\\\\\\\\\\)*[!><!]\\|\\\\+[^!><!]\\)+\\)!>\\(?:<!\\([^z-a]+?\\)!>\\)?!>" nil t)
      (when (string-match-p "@nested:\\(.+\\)" (match-string 1))
	(let* ((label (match-string 1))
	       (edtext (save-match-data
			 (when (string-match "@nested:\\(.+\\)" label)
			   (match-string 1 label))))
	       (start1 (- (match-beginning 1) 4))
	       (end1 (+ (length "@nested:") (match-beginning 1)))
	       (start2 (- (match-beginning 2) 4))
	       (end2 (+ (match-end 2) 4)))
	  (add-text-properties start1 end1 '(invisible t))
	  (add-text-properties start2 end2 '(invisible t)))))))

;;;###autoload
(defun org-critical-edition-insert-critical-note ()
  "Open the edit buffer for insert or edit a critical note. Point
must be on a marked region or an edited passage."
  (interactive)
  (cond ((region-active-p)
	 (setq org-critical-edition-origin-buffer (current-buffer))
	 (setq org-critical-edition-current-w-conf (random 1000))
	 (window-configuration-to-register org-critical-edition-current-w-conf)
	 (setq edtext (concat "edtext:"
			      (buffer-substring-no-properties (region-beginning) (region-end))))
	 (when (get-buffer "edit-critical-note")
	   (kill-buffer "edit-critical-note"))
	 (get-buffer-create "edit-critical-note")
	 (set-buffer "edit-critical-note")
	 (org-mode)
	 (unless (bound-and-true-p org-critical-edition-mode)
	   (org-critical-edition-mode))
	 (pop-to-buffer "edit-critical-note")
	 (setq header-line-format (format
				   "Edit: %s"
				   (replace-regexp-in-string "edtext:" "" edtext)))
	 (highlight-regexp "\\(:latex\\|:lemma\\|;;\\|:[A-Z]+\\)" 'hi-red-b))
	;; edit a note
	((org-in-regexp org-link-bracket-re 100)
	 (setq org-critical-edition-origin-buffer (current-buffer))
	 (setq org-critical-edition-current-w-conf (random 1000))
	 (window-configuration-to-register org-critical-edition-current-w-conf)
	 (setq edtext (replace-regexp-in-string "edtext:" "" (match-string 1)))
	 (setq critical-note (match-string 2))
	 (when (get-buffer "edit-critical-note")
	   (kill-buffer "edit-critical-note"))
	 (get-buffer-create "edit-critical-note")
	 (set-buffer "edit-critical-note")
	 (org-mode)
	 (setq header-line-format (format
				   "Edit: %s"
				   (replace-regexp-in-string "edtext:" "" edtext)))
	 (unless (bound-and-true-p org-critical-edition-mode)
	   (org-critical-edition-mode))
	 (pop-to-buffer "edit-critical-note")
	 (insert critical-note)
	 ;; arrange notes on several lines for easier reading.
	 (org-critical-edition-replace "\\(;;\s*\\)" "\\1\n")	   
	 (org-critical-edition-replace "\\(:nested\\)" "\n\\1")
	 (org-critical-edition-replace "\\(:!(\\)" "\n\\1")	   
	 (highlight-regexp "\\(:latex\\|:lemma\\|;;\\|:[A-Z]+\\)" 'hi-red-b))))

;;;###autoload
(defun org-critical-edition-kill-edit-buffer-and-save ()
  "Save and kill the edit buffer and return point to the edited
text."
  (interactive)
  (pcase (buffer-name (current-buffer))
    ("edit-critical-note"
     (org-critical-edition-replace "\n\n+" "\n")
     (save-excursion
       (goto-char (point-max))
       (when (looking-at-p "^$")
	 (re-search-backward ".")
	 (end-of-line)
	 (delete-blank-lines)
	 (delete-forward-char 1)))
     (setq org-critical-edition-buf-edit (buffer-substring (point-min) (point-max)))
     (jump-to-register org-critical-edition-current-w-conf)
     (switch-to-buffer org-critical-edition-origin-buffer)
     (cond ((region-active-p)
	    (save-excursion
	      (save-restriction
		(narrow-to-region (region-beginning) (region-end))
		(delete-region (point-min) (point-max))
		(insert (format "[[%s][%s]]" edtext org-critical-edition-buf-edit)))))
	   ((org-in-regexp org-link-bracket-re 100)
	    (save-restriction
	      (narrow-to-region (match-beginning 0) (match-end 0))
	      (save-excursion
		(goto-char (point-min))
		(when
		    (re-search-forward org-link-bracket-re nil t)
		  (let ((str (match-string 1)))
		    (delete-region (point-min) (point-max))
		    (insert (concat
			     "[["
			     str
			     "]["
			     org-critical-edition-buf-edit "]]")))))))))
    ;; when nested notes
    ("edit-nested-note"
     (org-critical-edition-replace "\n\n+" "\n")
     (save-excursion
       (goto-char (point-max))
       (when (looking-at-p "^$")
	 (re-search-backward ".")
	 (end-of-line)
	 (delete-blank-lines)
	 (delete-forward-char 1)))
     (setq org-critical-edition-buf-edit (buffer-substring (point-min) (point-max)))
     (jump-to-register org-critical-edition-current-w-conf)
     (switch-to-buffer org-critical-edition-origin-buffer)
     (cond ((region-active-p)
	    (save-excursion
	      (save-restriction
		(narrow-to-region (region-beginning) (region-end))
		(delete-region (point-min) (point-max))
		(insert (format "<!<!%s!><!%s!>!>" nested org-critical-edition-buf-edit)))))
	   ((org-in-regexp
	     "<!<!\\(\\(?:[^!><!\\]\\|\\\\\\(?:\\\\\\\\\\)*[!><!]\\|\\\\+[^!><!]\\)+\\)!>\\(?:<!\\([^z-a]+?\\)!>\\)?!>"
	     100)
	    (save-restriction
	      (narrow-to-region (match-beginning 0) (match-end 0))
	      (save-excursion
		(goto-char (point-min))
		(when
		    (re-search-forward
		     "<!<!\\(\\(?:[^!><!\\]\\|\\\\\\(?:\\\\\\\\\\)*[!><!]\\|\\\\+[^!><!]\\)+\\)!>\\(?:<!\\([^z-a]+?\\)!>\\)?!>"
		     nil t)
		  (let ((str (match-string 1)))
		    (delete-region (point-min) (point-max))
		    (insert (concat
			     "<!<!"
			     str
			     "!><!"
			     org-critical-edition-buf-edit "!>!>")))))))))))

;;;###autoload
(defun org-critical-edition-kill-edit-buffer ()
  (interactive)
  (jump-to-register org-critical-edition-current-w-conf)
  (switch-to-buffer org-critical-edition-origin-buffer)
  (deactivate-mark))

;;;###autoload
(defun org-critical-edition-posframe ()
  "Display the content of a critical note in a postframe. Point
must be in an edited passage."
  (interactive)
  (if (not (org-in-regexp org-link-bracket-re 100))
      (error "Not in a critical note!")
    (let ((critical-note (match-string 2)))
      (with-temp-buffer
	(insert critical-note)
	(org-mode)
	(mark-whole-buffer)
	(org-fill-paragraph)
	(deactivate-mark)
	(setq critical-note-posframe (buffer-substring-no-properties (point-min) (point-max))))
      (when (posframe-workable-p)
	(posframe-show "critical-note-posframe"
		       :string critical-note-posframe
		       :internal-border-color "blue"
		       :internal-border-width 2
		       :background-color "darkslategray"
		       :foreground-color "white")))))

;;;###autoload
(defun org-critical-edition-delete-posframe ()
  "Delete the posframe that displays the critical notes at
point."
  (interactive)
  (posframe-delete "critical-note-posframe"))

;;;###autoload
(defun org-critical-edition-remove-note ()
  "Remove all critical notes at point and stop editing the
passage."
  (interactive)
  (cond ((not (org-in-regexp org-link-bracket-re 100))
	 (error "Not in a critical note!"))
	((org-in-regexp org-link-bracket-re 100)
	 (save-restriction
	   (narrow-to-region (match-beginning 0) (match-end 0))
	   ;; (remove-overlays nil nil '((edtext-ov1 t) (edtext-ov2 t)))
	   (save-excursion
	     (goto-char (point-min))
	     (when
		 (re-search-forward org-link-bracket-re nil t)
	       (let ((str (replace-regexp-in-string
			   "edtext:" ""
			   (match-string 1))))
		 (delete-region (point-min) (point-max))
		 (insert str))))))))

;;;###autoload
(defun org-critical-edition-insert-nested-note ()
  "Open the edit buffer for insert or edit a nested critical
note. Point must be on a marked region or an edited passage."
  (interactive)
  (cond ((region-active-p)
	 (setq org-critical-edition-origin-buffer (current-buffer))
         (setq org-critical-edition-current-w-conf (random 1000))
         (window-configuration-to-register org-critical-edition-current-w-conf)
	 (setq nested (concat "@nested:"
			      (buffer-substring-no-properties (region-beginning) (region-end))))
	 (when (get-buffer "edit-nested-note")
	   (kill-buffer "edit-nested-note"))
	 (get-buffer-create "edit-nested-note")
	 (set-buffer "edit-nested-note")
	 (org-mode)
	 (unless (bound-and-true-p org-critical-edition-mode)
	   (org-critical-edition-mode))
	 (pop-to-buffer "edit-nested-note")
	 (highlight-regexp "\\(:latex\\|:lemma\\|;;\\|:[A-Z]+\\)" 'hi-red-b))
	;; edit a note
	((org-in-regexp
	  "<!<!\\(\\(?:[^!><!\\]\\|\\\\\\(?:\\\\\\\\\\)*[!><!]\\|\\\\+[^!><!]\\)+\\)!>\\(?:<!\\([^z-a]+?\\)!>\\)?!>"
	  100)
	 (setq org-critical-edition-origin-buffer (current-buffer))
         (setq org-critical-edition-current-w-conf (random 1000))
         (window-configuration-to-register org-critical-edition-current-w-conf)
	 (setq critical-note (match-string 2))
	 (when (get-buffer "edit-nested-note")
	   (kill-buffer "edit-nested-note"))
	 (get-buffer-create "edit-nested-note")
	 (set-buffer "edit-nested-note")
	 (org-mode)
	 (unless (bound-and-true-p org-critical-edition-mode)
	   (org-critical-edition-mode))
	 (pop-to-buffer "edit-nested-note")
	 (insert critical-note)
	 ;; arrange notes on several lines for easier reading.
	 (save-excursion
	   (goto-char (point-min))
	   (while (re-search-forward "\\(;;\s*\\)" nil t)
	     (replace-match "\\1\n" t nil)))
	 (highlight-regexp "\\(:latex\\|:lemma\\|;;\\|:[A-Z]+\\)" 'hi-red-b))))

;;;###autoload
(defun org-critical-edition-remove-nested-note ()
  "Remove all nested notes at point and stop editing the
passage."
  (interactive)
  (cond ((not
	  (org-in-regexp
	   "<!<!\\(\\(?:[^!><!\\]\\|\\\\\\(?:\\\\\\\\\\)*[!><!]\\|\\\\+[^!><!]\\)+\\)!>\\(?:<!\\([^z-a]+?\\)!>\\)?!>"
	   100))
	 (error "Not in a nested note!"))
	((org-in-regexp
	  "<!<!\\(\\(?:[^!><!\\]\\|\\\\\\(?:\\\\\\\\\\)*[!><!]\\|\\\\+[^!><!]\\)+\\)!>\\(?:<!\\([^z-a]+?\\)!>\\)?!>"
	  100)
	 (save-restriction
	   (narrow-to-region (match-beginning 0) (match-end 0))
	   ;; (remove-overlays nil nil '((edtext-ov1 t) (edtext-ov2 t)))
	   (save-excursion
	     (goto-char (point-min))
	     (when
		 (re-search-forward
		  "<!<!\\(\\(?:[^!><!\\]\\|\\\\\\(?:\\\\\\\\\\)*[!><!]\\|\\\\+[^!><!]\\)+\\)!>\\(?:<!\\([^z-a]+?\\)!>\\)?!>"
		  nil t)
	       (let ((str (replace-regexp-in-string
			   "@nested:" ""
			   (match-string 1))))
		 (delete-region (point-min) (point-max))
		 (insert str))))))))

(defun org-critical-edition-insert-current-edtext ()
  (interactive)
  (let ((edtext (replace-regexp-in-string
		 "edtext:"
		 ""
		 edtext)))
    (insert edtext)))

;;;###autoload
(defun org-critical-edition-filters/edit-section-autopar (tree backend info)
  (when (org-export-derived-backend-p backend 'latex)
    (org-element-map tree 'headline
      (lambda (section)
	(when (member "reledmac_autopar" (org-element-property :tags section))
	  (let ((create-export-snippet
		 ;; Create "latex" export-snippet with value v.
		 ;; Suggestion from Nicolas Goaziou.
		 (lambda (v)
		   (org-element-create 'export-snippet (list :back-end "latex" :value v)))))
	    (apply #'org-element-set-contents
		   section
		   (append (list (funcall create-export-snippet "\\beginnumbering\\autopar\n"))
			   (org-element-contents section)
			   (list (funcall create-export-snippet "\n\\endnumbering\n")))))))
      info)
    tree))

;;;###autoload
(defun org-critical-edition-filters/edit-section-par (tree backend info)
  (when (org-export-derived-backend-p backend 'latex)
    (org-element-map tree 'headline
      (lambda (section)
	(when (member "reledmac_par" (org-element-property :tags section))
	  (let ((create-export-snippet
		 ;; Create "latex" export-snippet with value v.
		 ;; Suggestion from Nicolas Goaziou.
		 (lambda (v)
		   (org-element-create 'export-snippet (list :back-end "latex" :value v)))))
	    (apply #'org-element-set-contents
		   section
		   (append (list (funcall create-export-snippet "\\beginnumbering\n"))
			   (org-element-contents (org-element-map section 'paragraph
						   (lambda (par)
						     (let ((create-export-snippet
							    (lambda (v)
							      (org-element-create 'export-snippet (list :back-end "latex" :value v)))))
						       (apply #'org-element-set-contents
							      par
							      (append (list (funcall create-export-snippet "\n\\pstart\n"))
								      (org-element-contents par)
								      (list (funcall create-export-snippet "\n\\pend\n"))))))
						   info))
			   (list (funcall create-export-snippet "\n\\endnumbering\n")))))))
      info)
    tree))

;;;###autoload
(defun org-critical-edition-filters/numbered-section (text backend info)
  (interactive)
  (when (org-export-derived-backend-p backend 'latex)
    (with-temp-buffer
      (insert text)
      (save-excursion
	(goto-char (point-min))
	(while (re-search-forward "\\[startnum\\]" nil t)
	  (let ((from (point))
		(to (save-excursion
		      (re-search-forward "\\[stopnum\\]" nil t)
		      (beginning-of-line)
		      (point))))
	    (save-restriction
	      (narrow-to-region from to)
	      (whitespace-cleanup)
	      (save-excursion
		(goto-char (point-max))
		(when (looking-at-p "^$")
		  (re-search-backward ".")
		  (end-of-line)
		  (delete-blank-lines)
		  (delete-forward-char 1)))
	      (org-critical-edition-replace "\n\n\n+" "\n\n")
	      (org-critical-edition-replace "^$" "\\\\pend\\\\pstart")
	      (save-excursion
		(goto-char (point-max))
		(newline 2)
		(insert "\\pend\\endnumbering"))
	      (save-excursion
		(goto-char (point-min))
		(insert "\\beginnumbering\\pstart")
		(newline 2))))))
      (org-critical-edition-replace "\\[startnum\\]\\|\\[stopnum\\]" "")
      (setq text (buffer-string)))))

(defvar org-critical-edition-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c C-x '") 'org-critical-edition-insert-critical-note)
    (define-key map (kbd "C-c C-x `") 'org-critical-edition-notes-to-nested-in-region)
    (define-key map (kbd "C-c C-x =") 'org-critical-edition-insert-nested-note)
    (define-key map (kbd "C-c C-x s") 'org-critical-edition-kill-edit-buffer-and-save)
    (define-key map (kbd "C-c C-x k") 'org-critical-edition-kill-edit-buffer)
    (define-key map (kbd "C-c C-x ?") 'org-critical-edition-posframe)
    (define-key map (kbd "C-c C-x :") 'org-critical-edition-delete-posframe)
    (define-key map (kbd "C-c C-x (") 'org-critical-edition-show-notes)
    (define-key map (kbd "C-c C-x )") 'org-critical-edition-hide-notes)
    (define-key map (kbd "C-c C-x -") 'org-critical-edition-remove-note)
    (define-key map (kbd "C-c C-x _") 'org-critical-edition-remove-nested-note)
    (define-key map (kbd "C-c C-x e") 'org-critical-edition-insert-current-edtext)
    map))

(add-to-list 'minor-mode-map-alist `(org-critical-edition-mode . ,org-critical-edition-mode-map) t)

;;; org-critical-edition.el ends here
